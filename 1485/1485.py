# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division

from sys import stdin

entrada = list(stdin.read().strip())
entrada.sort()
print(''.join(entrada))

#for c in entrada:
#    print(c, end='')
#print()
