# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division

from sys import stdin

entrada = stdin.read().split()
letras = entrada[-1]
nums = [int(x) for x in entrada[:3]]
nums.sort()
d = dict(zip('ABC', nums))

r = []
for c in letras:
    r.append(d[c])

print(' '.join([str(x) for x in r]))