# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division

from sys import stdin

nums = iter([int(x) for x in stdin.read().split()])
n = next(nums)
si = set()
no = set()
for i in range(n):
    inicio = next(nums)
    fin = next(nums)
    r = []
    for j in range(inicio, fin + 1):
        if j in si:
            r.append(str(j))
        elif j not in no:
            b = bin(j)[2:]
            if b == b[::-1]:
                r.append(str(j))
                si.add(j)
            else:
                no.add(j)
    print(' '.join(r))
