# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division

from sys import stdin

nums = iter([int(x) for x in stdin.read().split()])

T = next(nums)
for i in range(T):
    N = next(nums)
    s = 0
    for j in range(N):
        x = next(nums)
        s += x
    if s % N == 0:
        print('YES')
    else:
        print('NO')
