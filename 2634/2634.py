# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division

from sys import stdin

entrada = iter(stdin.read().split())

n = int(next(entrada))

meses = [0] * 13

for e in range(n):
    ID = next(entrada)
    fecha = next(entrada)
    dia, mes, anio = fecha.split('/')
    mes = int(mes)
    meses[mes] += 1
for i in range(1, 13):
    print(i, meses[i])