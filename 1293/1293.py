# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import division

from sys import stdin

N = int(stdin.read())
print(2 ** N)