# Programación competitiva -- División de bronce

Este repositorio contiene los archivos correspondientes a la división de bronce del grupo de programación competitiva del 
Tecnológico de Monterrey, Campus Estado de México, correspondientes al semestre enero-mayo del 2017.

- 10 de marzo, 2017. 
    - [Problema COJ 1484 Hotest Mountain](http://coj.uci.cu/24h/problem.xhtml?pid=1484)
- 17 de marzo, 2017.
    - [Problema COJ 1101 Binaries Palindromes](http://coj.uci.cu/24h/problem.xhtml?pid=1101)
- 24 de marzo, 2017.
    - [Problema COJ 1300 Modulo](http://coj.uci.cu/24h/problem.xhtml?pid=1300)
- 21 de abril, 2017.
    - [Problema COJ 2634 Birthday Statistics](http://coj.uci.cu/24h/problem.xhtml?pid=2634)
    - [Problema COJ 1626 Adding Reversed Numbers](http://coj.uci.cu/24h/problem.xhtml?pid=1626)