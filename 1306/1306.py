# coding: utf-8

from __future__ import print_function
from __future__ import division

from sys import stdin

entrada = stdin.read().split()
enteros = [int(c) for c in entrada]
t = enteros[0]
for n in enteros[1:]:
    if n % 4 == 0:
        print('YES')
    else:
        print('NO')

    
